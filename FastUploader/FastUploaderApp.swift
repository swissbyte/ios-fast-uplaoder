//
//  FastUploaderApp.swift
//  FastUploader
//
//  Created by Claudio Hediger on 23.04.23.
//

import SwiftUI

@main
struct FastUploaderApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
