//
//  ContentView.swift
//  FastUploader
//
//  Created by Claudio Hediger on 23.04.23.
//

import SwiftUI
import Foundation
import Photos

var sentImages = 0
var apiURL = "http://192.168.30.84:5050"


class GlobalData: ObservableObject {
    @Published var sentImages: Int = 0
}



struct ContentView: View {
    
    @StateObject var globalData = GlobalData()
    
    @State private var ipAddresses = ["192.168.30.84", "192.168.30.85"]
    @State private var selectedIpAddress = "192.168.30.84"
    
    @State private var payloadText = ""
    
    
    
    var body: some View {
        VStack {
            Text("Fast Uploader!")
                .font(.largeTitle)
            
            Divider()
                .frame(height: 50)

            HStack{
                Text("Choose Server: ")
                Divider()
                    .frame(height: 20)
                Picker("IP Address", selection: $selectedIpAddress) {
                    ForEach(ipAddresses, id: \.self) { ipAddress in
                        Text(ipAddress)
                    }
                }
                .pickerStyle(MenuPickerStyle())
                
            }
            
            
            Button(action: {
                apiURL = "http://"+String(selectedIpAddress)+":5050"
                postAllAssetURLs()
                
            }){ label: do {
                HStack {
                    Image(systemName: "arrow.up.square.fill")
                    Text("Upload to: "+String(selectedIpAddress))
                    Divider()
                    Text("\(globalData.sentImages)")
                }
                .fixedSize()
                .frame(maxWidth: .infinity)
            }}
            .buttonStyle(.borderedProminent)
            .padding(.bottom)
            
            Button(action: {
                let url = "http://\(selectedIpAddress):5050/save"
                postRequest(url: url, payload: payloadText)
            }) {
                Text("Test HTTP")
                    .frame(maxWidth: .infinity)
            }
            .buttonStyle(.borderedProminent)
            
        }
        .padding()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}



func postRequest(url: String, payload: String) {
    guard let url = URL(string: url) else {
        print("Invalid URL: \(url)")
        return
    }
    
    var request = URLRequest(url: url)
    request.httpMethod = "POST"
    request.setValue("application/json", forHTTPHeaderField: "Content-Type")
    
    let data = payload.data(using: .utf8)
    request.httpBody = data
    
    let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
        guard error == nil else {
            print("Error: \(error!)")
            return
        }
        
        guard let data = data else {
            print("No data returned")
            return
        }
        
        if let responseString = String(data: data, encoding: .utf8) {
            print("Response: \(responseString)")
        } else {
            print("Invalid response data")
        }
    }
    
    task.resume()
}

func postImage(url: String, imageData: Data, filename: String, albumTitle: String) {
    guard let url = URL(string: url) else {
        print("Invalid URL: \(url)")
        return
    }
    
    let boundary = UUID().uuidString
    let contentType = "multipart/form-data; boundary=\(boundary)"
    
    var request = URLRequest(url: url)
    request.httpMethod = "POST"
    request.setValue(contentType, forHTTPHeaderField: "Content-Type")
    request.setValue(albumTitle, forHTTPHeaderField: "album")
    
    let body = NSMutableData()
    
    body.append("--\(boundary)\r\n".data(using: .utf8)!)
    body.append("Content-Disposition:form-data; name=\"file\"; filename=\"\(filename)\"\r\n".data(using: .utf8)!)
    body.append("Content-Type: application/octet-stream\r\n\r\n".data(using: .utf8)!)
    body.append(imageData)
    body.append("\r\n".data(using: .utf8)!)
    body.append("--\(boundary)--\r\n".data(using: .utf8)!)
    
    request.httpBody = body as Data
    
    let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
        guard error == nil else {
            print("Error: \(error!)")
            return
        }
        
        guard let data = data else {
            print("No data returned")
            return
        }
        
        if let responseString = String(data: data, encoding: .utf8) {
            print("Response: \(responseString)")
        } else {
            print("Invalid response data")
        }
    }
    
    task.resume()
}




func postAllAssetURLs() {
    let fetchOptions = PHFetchOptions()
    let allAssets = PHAsset.fetchAssets(with: fetchOptions)
    
    for index in 0..<allAssets.count {
        let asset = allAssets[index]
        
        while (index - sentImages) > 100 {
            // Wait for currently sent images to complete
            RunLoop.current.run(until: Date(timeIntervalSinceNow: 1.0))
            print("Wait for Images to be sent... Index: "+String(sentImages))
        }
        autoreleasepool { // make sure memory is freed, otherwise a few big files
            if asset.mediaType == .image || asset.mediaType == .video {
                let options = PHImageRequestOptions()
                options.isSynchronous = true
                options.deliveryMode = .highQualityFormat
                options.isNetworkAccessAllowed = true
                var albumTitle = "general"
                let resources = PHAssetResource.assetResources(for: asset)
                if let originalFilename = resources.first?.originalFilename
                {
                    let assetCollection = PHAssetCollection.fetchAssetCollectionsContaining(asset, with: .album, options: nil)
                    
                    albumTitle = assetCollection.firstObject?.localizedTitle ?? "general"
                    
                    PHImageManager.default().requestImageDataAndOrientation(for: asset, options: options) { data, _, _, info in
                        guard let imageData = data else { return }
                        let filename = String(index) + "_" + originalFilename
                        let urlString = apiURL + "/save"
                        
                        print("Upload File: "+filename+" for Album: "+albumTitle)
                        
                        checkFileExists(url: apiURL + "/check_file_exists", filename: albumTitle+"/"+filename, callback: {
                            
                            postImage(url: urlString, imageData: imageData, filename: filename, albumTitle: albumTitle)
                            
                        })
                    }
                }
            }
        }
    }
}




func checkFileExists(url: String, filename: String, callback: @escaping () -> Void) {
    guard let url = URL(string: url) else {
        print("Invalid URL: \(url)")
        return
    }
    
    
    
    var request = URLRequest(url: url)
    request.httpMethod = "POST"
    request.setValue("application/json", forHTTPHeaderField: "Content-Type")
    
    let json: [String: Any] = ["filename": filename]
    let jsonData = try? JSONSerialization.data(withJSONObject: json)
    
    request.httpBody = jsonData
    
    let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
        guard error == nil else {
            print("Error: \(error!)")
            return
        }
        
        guard let data = data else {
            print("No data returned")
            return
        }
        
        if let responseString = String(data: data, encoding: .utf8) {
            print(responseString)
            sentImages = sentImages + 1
            GlobalData().sentImages += 1
            if responseString == "file_not_found" {
                callback()
            }
        } else {
            print("Invalid response data")
        }
    }
    
    task.resume()
}






